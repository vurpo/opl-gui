use std::sync::mpsc::{
    Sender
};
use eframe::{egui, epi};
use crate::instrument::{
    TwoOpInstrument,
    Operator
};
pub struct OplApp {
    pub message_tx: Sender<crate::Message>,
    pub instrument: TwoOpInstrument
}

impl epi::App for OplApp {
    fn name(&self) -> &str {
        "egui template"
    }

    /// Called once before the first frame.
    fn setup(
        &mut self,
        _ctx: &egui::CtxRef,
        _frame: &mut epi::Frame<'_>,
        _storage: Option<&dyn epi::Storage>,
    ) {
    }

    fn update(&mut self, ctx: &egui::CtxRef, frame: &mut epi::Frame<'_>) {

        let mut instrument_changed = false;

        egui::CentralPanel::default().show(ctx, |ui| {
            egui::Window::new("Operator")
                .show(ctx, |ui| {

                    instrument_changed = instrument_changed || ui.add(egui::Checkbox::new(&mut self.instrument.operators[0].sustaining, "Op0 sustain")).changed();
                    instrument_changed = instrument_changed || ui.add(egui::Checkbox::new(&mut self.instrument.operators[1].sustaining, "Op1 sustain")).changed();

                    instrument_changed = instrument_changed || ui.add(egui::Slider::new(&mut self.instrument.operators[0].attack, 0..=15).clamp_to_range(true)).changed();
                    instrument_changed = instrument_changed || ui.add(egui::Slider::new(&mut self.instrument.operators[0].decay, 0..=15).clamp_to_range(true)).changed();
                    instrument_changed = instrument_changed || ui.add(egui::Slider::new(&mut self.instrument.operators[0].sustain, 0..=15).clamp_to_range(true)).changed();
                    instrument_changed = instrument_changed || ui.add(egui::Slider::new(&mut self.instrument.operators[0].release, 0..=15).clamp_to_range(true)).changed();

                    instrument_changed = instrument_changed || ui.add(egui::Slider::new(&mut self.instrument.operators[1].attack, 0..=15).clamp_to_range(true)).changed();
                    instrument_changed = instrument_changed || ui.add(egui::Slider::new(&mut self.instrument.operators[1].decay, 0..=15).clamp_to_range(true)).changed();
                    instrument_changed = instrument_changed || ui.add(egui::Slider::new(&mut self.instrument.operators[1].sustain, 0..=15).clamp_to_range(true)).changed();
                    instrument_changed = instrument_changed || ui.add(egui::Slider::new(&mut self.instrument.operators[1].release, 0..=15).clamp_to_range(true)).changed();
                });
        });

        if instrument_changed {
            self.message_tx.send(crate::Message::InstrumentMessage(self.instrument.clone()));
        }
    }
}