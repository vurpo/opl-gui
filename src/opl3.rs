use serialport::SerialPort;

use crate::instrument::{FourOpInstrument, Operator, TwoOpInstrument};

// base register + this[operator number] = register for that operator number
const OPERATOR_OFFSETS: [u16; 36] = [
    0x000, 0x001, 0x002, 0x003, 0x004, 0x005, 0x008, 0x009, 0x00A, 0x00B, 0x00C, 0x00D, 0x010, 0x011, 0x012, 0x013, 0x014, 0x015,
    0x100, 0x101, 0x102, 0x103, 0x104, 0x105, 0x108, 0x109, 0x10A, 0x10B, 0x10C, 0x10D, 0x110, 0x111, 0x112, 0x113, 0x114, 0x115
];

// base register + this[channel number] = register for that channel number
const CHANNEL_OFFSETS: [u16; 18] = [
    0x000, 0x001, 0x002, 0x003, 0x004, 0x005, 0x006, 0x007, 0x008,
    0x100, 0x101, 0x102, 0x103, 0x104, 0x105, 0x106, 0x107, 0x108
];

// which operators map to which 4-op channels (match statement as a "lookup table" because the indices are sparse, not contiguous)
const fn fourop_operators(channel: u8) -> Option<[u8; 4]> {
    match channel {
        0 => Some([0, 3, 6, 9]),
        1 => Some([1, 4, 7, 10]),
        2 => Some([2, 5, 8, 11]),

        9 => Some([18, 21, 24, 27]),
        10 => Some([19, 22, 25, 28]),
        11 => Some([20, 23, 26, 29]),

        _ => None
    }
}

// which operators map to which 2-op channels
const TWOOP_OPERATORS: [[u8; 2]; 18] = [
    [0, 3], [1, 4], [2, 5], [6, 9], [7, 10], [8, 11], [12, 15], [13, 16], [14, 17], [18, 21], [19, 22], [20, 23], [24, 27], [25, 28], [26, 29], [30, 33], [31, 34], [32, 35]
];

pub struct OPL3 {
    twoop_channels: Vec<u8>,
    fourop_channels: Vec<u8>,
    shadow_register: [u8; 512],
    shadow_register_dirty: [bool; 512], // true if the shadow register has been set without being written out

    output: Box<dyn OPL3Output>
}

impl OPL3 {
    pub fn new(output: Box<dyn OPL3Output>) -> OPL3 {
        let mut opl3 = OPL3 {
            twoop_channels: Vec::new(),
            fourop_channels: Vec::new(),
            shadow_register: [0; 512],
            shadow_register_dirty: [false; 512],

            output: output
        };

        opl3.reset();
        opl3.init_opl3();

        opl3
    }

    /// Performs a few OPL3 initialization functions
    pub fn init_opl3(&mut self) {
        // set the OPL3 bit to enable OPL3 features
        self.set_register(0x105, 1);
        // set waveform select enable, to be able to use all waveforms
        self.set_register(0x01, 1<<5);
        self.flush_registers();
    }

    /// Sets many instruments on the OPL3. Channels that don't get set here will remain what they were previously!
    /// You must use this function if you want to change how many 4-op and 2-op instruments are in use.
    /// If you only want to change individual instruments without changing the operator configuration, use set_twoop_instrument and set_fourop_instrument
    /// If successful, returns which channels are assigned to which of the given 4-op and 2-op instruments.
    /// Fails if given more instruments than is possible to assign to a single OPL3.
    pub fn set_instruments(&mut self, percussion_enabled: bool, twoop_instruments: &[TwoOpInstrument], fourop_instruments: &[FourOpInstrument]) -> Result<(Vec<u8>, Vec<u8>), ()> {
        // only one or zero percussion is possible
        //self.percussion_enabled = percussion_enabled;
        if percussion_enabled {
            self.change_register(0xBD, |v| v|0b00100000); // bit 5 of 0xBD = enable percussion mode
            println!("Percussion is enabled");
        } else {
            self.change_register(0xBD, |v| v&0b11011111);
            println!("Percussion not enabled");
        }

        // max number of 4-op instruments is always 6
        if fourop_instruments.len() > 6 {
            return Err(())
        } else {
            self.fourop_channels = [0, 1, 2, 9, 10, 11][0..fourop_instruments.len()].to_vec();

            // actually set the instruments
            for (instrument, channel) in fourop_instruments.iter().zip(self.fourop_channels.clone()) {
                self.set_channel_note(channel, false, 0, 0);
                self.set_fourop_instrument(channel, instrument);
            }
        }
        println!("Channels assigned to 4-op instruments: {:?}", self.fourop_channels);

        // max number of possible 2-op instruments is now determined by the percussion and number of 4-op instruments
        // each 4-op instrument loses 4 operators (= 2 2-op instruments) and percussion enabled loses 6 operators (= 3 2-op instruments)
        let max_twoop_instruments = 18 - 2*fourop_instruments.len() - (if percussion_enabled {3} else {0});
        if twoop_instruments.len() > max_twoop_instruments {
            return Err(())
        } else {
            // first, some logic to deduce which channels can be used for 2-op instruments
            let mut channels_to_remove: Vec<u8> = Vec::new();
            for &c in &self.fourop_channels {
                channels_to_remove.push(c);
                channels_to_remove.push(c+3);
            }
            if percussion_enabled {
                channels_to_remove.push(6);
                channels_to_remove.push(7);
                channels_to_remove.push(8);
            }
            self.twoop_channels = (0..18).filter(|n| !channels_to_remove.contains(n)).take(twoop_instruments.len()).collect();

            // actually set the instruments
            for (instrument, channel) in twoop_instruments.iter().zip(self.twoop_channels.clone()) {
                self.set_channel_note(channel, false, 0, 0);
                self.set_twoop_instrument(channel, instrument);
            }
        }
        println!("Channels assigned to 2-op instruments: {:?}", self.twoop_channels);

        Ok((self.fourop_channels.clone(), self.twoop_channels.clone()))
    }

    pub fn set_operator(&mut self, number: u8, operator: &Operator) {
        let offset = OPERATOR_OFFSETS[number as usize];

        // 0x20+offset: tremolo, vibrato, sustain, KSR, frequency multiplication factor
        let value =
            (operator.tremolo as u8)<<7 |
            (operator.vibrato as u8)<<6 |
            (operator.sustaining as u8)<<5 |
            (operator.key_scale as u8)<<4 |
            (operator.frequency_multiplier & 0b1111);
        self.set_register(0x20+offset, value);

        // 0x40+offset: key scale level, output level
        let value =
            (operator.key_scale & 0b11)<<6 |
            (operator.output_level_attenuation & 0b111111);
        self.set_register(0x40+offset, value);

        // 0x60+offset: attack, decay
        let value =
            (operator.attack & 0b1111)<<4 |
            (operator.decay & 0b1111);
        self.set_register(0x60+offset, value);

        // 0x80+offset: sustain, release
        let value =
            (operator.sustain & 0b1111)<<4 |
            (operator.release & 0b1111);
        self.set_register(0x80+offset, value);

        // 0xE0: waveform
        self.set_register(0xE0+offset, operator.waveform as u8 & 0b111);

        self.flush_registers();
    }

    /// Change one of the currently defined 2-operator instruments.
    pub fn set_twoop_instrument(&mut self, channel: u8, instrument: &TwoOpInstrument) -> Result<(), ()> {
        if !self.twoop_channels.contains(&channel) { return Err(()) } // chosen channel is not currently a 2-op channel

        // set 2-op mode, stereo and feedback bits on channel
        let value: u8 = (((instrument.stereo_channels as u8) & 0b11) << 4) | ((instrument.feedback & 0b111) << 1) | (instrument.mode as u8);
        self.set_register(0xC0+CHANNEL_OFFSETS[channel as usize], value);
        self.flush_registers();

        // set operator values
        self.set_operator(TWOOP_OPERATORS[channel as usize][0], &instrument.operators[0]);
        self.set_operator(TWOOP_OPERATORS[channel as usize][1], &instrument.operators[1]);

        Ok(())
    }

    pub fn set_fourop_instrument(&mut self, channel: u8, instrument: &FourOpInstrument) -> Result<(), ()> {
        if !self.fourop_channels.contains(&channel) { return Err(()) } // chosen channel is not currently a 4-op channel

        // set 4-op mode, stereo and feedback bits on channel
        let value: u8 = (((instrument.stereo_channels as u8) & 0b11) << 4) | ((instrument.feedback & 0b111) << 1) | (instrument.mode as u8 & 1);
        self.set_register(0xC0+CHANNEL_OFFSETS[channel as usize], value);
        self.change_register(0xC0+CHANNEL_OFFSETS[channel as usize + 3], |v| v|((instrument.mode as u8 & 0b10) >> 1));
        self.flush_registers();

        // set operator values
        self.set_operator(fourop_operators(channel).unwrap()[0], &instrument.operators[0]);
        self.set_operator(fourop_operators(channel).unwrap()[1], &instrument.operators[1]);
        self.set_operator(fourop_operators(channel).unwrap()[2], &instrument.operators[2]);
        self.set_operator(fourop_operators(channel).unwrap()[3], &instrument.operators[3]);

        Ok(())
    }

    pub fn set_channel_note(&mut self, channel: u8, key_on: bool, block_number: u8, f_number: u16) {
        let f_number_bytes = f_number.to_be_bytes(); // high byte, low byte!
        let offset = CHANNEL_OFFSETS[channel as usize];

        self.set_register(0xA0+offset, f_number_bytes[1]);
        self.set_register(0xB0+offset, ((key_on as u8) << 5) | ((block_number & 0b111) << 2) | (f_number_bytes[0] & 0b11));
        self.flush_registers();
    }

    pub fn note_off(&mut self, channel: u8) {
        self.change_register(0xB0+CHANNEL_OFFSETS[channel as usize],|v| v&0b11011111);
        self.flush_registers();
    }

    /// Sets key on to 0 for all channels (turning off all sound)
    pub fn all_off(&mut self) {
        for i in 0..18 {
            self.change_register(0xB0+CHANNEL_OFFSETS[i], |v| v&0b11011111);
        }
        self.flush_registers();
    }

    /// Set the 8-bit register at address to value. Sets the shadow register, does not write out to OPL3.
    pub fn set_register(&mut self, address: u16, value: u8) {
        self.change_register(address, |_| value);
    }

    /// Modifies the 8-bit register at address using the function op. Useful if you want to change less than the full 8 bits.  Sets the shadow register, does not write out to OPL3.
    pub fn change_register<F>(&mut self, address: u16, op: F) where F: Fn(u8) -> u8 {
        let new_value = op(self.shadow_register[address as usize]);
        if new_value != self.shadow_register[address as usize] { // set this address to "dirty" only if the byte changed
            self.shadow_register_dirty[address as usize] = true;
        }
        self.shadow_register[address as usize] = new_value;
    }

    /// Writes all *modified* (since the last flush) bytes in the shadow register out to the real OPL3.
    pub fn flush_registers(&mut self) {
        for i in 0..self.shadow_register.len() {
            if self.shadow_register_dirty[i] { // "dirty" means it has changed, so write it out now
                self.write_register_direct(i as u16, self.shadow_register[i]);
                self.shadow_register_dirty[i] = false;
            }
        }
    }

    /// Perform a direct write of value to the OPL3 register at address (does not touch the shadow register).
    pub fn write_register_direct(&mut self, address: u16, value: u8) {
        self.output.write(address, value);
    }

    /// Use the OPL3's hardware reset pin to reset the OPL3, and clears the shadow register
    pub fn reset(&mut self) {
        self.shadow_register = [0; 512];
        self.shadow_register_dirty = [false; 512];
        self.output.reset();
    }
}

pub trait OPL3Output {
    /// Write a byte to an address on the OPL3
    fn write(&mut self, address: u16, value: u8);

    /// Reset the OPL3 using its hardware reset line
    fn reset(&mut self);
}

pub struct VurpoSerialOPL3 {
    pub port: Box<dyn SerialPort>
}

impl OPL3Output for VurpoSerialOPL3 {
    fn write(&mut self, address: u16, value: u8) {
        // Evil bit twiddling, because I pack the bits along with some byte synchronization, into three bytes

        let byte0: u8 = ((address & 0b111111111)>>4) as u8;
        let byte1: u8 = 0b01000000 | (((address & 0b00001111)<<2) as u8) | value>>6;
        let byte2: u8 = 0b10000000 | (value & 0b00111111);
        
        if let Err(e) = self.port.write(&[byte0, byte1, byte2]) {
            eprintln!("Failed to write: {:?}", e);
        }
    }

    fn reset(&mut self) {
        if let Err(e) = self.port.write(&[0xFF]) {
            eprintln!("Failed to write: {:?}", e);
        }
    }
}