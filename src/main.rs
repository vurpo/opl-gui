use std::{thread, time};
use std::io::{stdin, stdout, Write};
use std::sync::mpsc::channel;
use midir::{MidiInput, Ignore};

use instrument::{
    TwoOpInstrument,
    FourOpInstrument,
    Operator
};
use opl3::OPL3;

use crate::opl3::VurpoSerialOPL3;

mod instrument;
mod music;
mod opl3;

mod gui;

fn key_number_to_f_number(key: u8) -> Option<(u8, u16)> {
    let octave = key / 12;
    let index = key % 12;
    if octave < 1 {
        None
    } else {
        Some((octave-1, music::F_NUMBERS_SCALE[index as usize]))
    }
}

pub enum Message {
    MidiMessage(Vec<u8>),
    InstrumentMessage(TwoOpInstrument)
}

fn main() {

    let mut midi_in = MidiInput::new("midir reading input").unwrap();
    midi_in.ignore(Ignore::None);
    
    // Get an input port (read from console if multiple are available)
    let in_ports = midi_in.ports();
    let in_port = match in_ports.len() {
        0 => panic!("no input port found"),
        1 => {
            println!("Choosing the only available input port: {}", midi_in.port_name(&in_ports[0]).unwrap());
            &in_ports[0]
        },
        _ => {
            println!("\nAvailable input ports:");
            for (i, p) in in_ports.iter().enumerate() {
                println!("{}: {}", i, midi_in.port_name(p).unwrap());
            }
            print!("Please select input port: ");
            stdout().flush().unwrap();
            let mut input = String::new();
            stdin().read_line(&mut input).unwrap();
            in_ports.get(input.trim().parse::<usize>().unwrap())
                     .ok_or("invalid input port selected").unwrap()
        }
    };
    
    println!("\nOpening connection");
    //let in_port_name = midi_in.port_name(in_port).unwrap();

    let mut last_key: u8 = 0;

    let (tx, rx) = channel();

    let tx_ = tx.clone();
    // _conn_in needs to be a named parameter, because it needs to be kept alive until the end of the scope
    let _conn_in = midi_in.connect(in_port, "midir-read-input", move |stamp, message, _| {
        if message.len() == 3 {
            if message[0] == 0x80 || message[0] == 0x90 {
                let message_: Vec<u8> = message.into();
                if let Err(e) = tx_.send(Message::MidiMessage(message_)) {
                    eprintln!("Nobody receiving MIDI message! {:?}", e);
                }
            }
        }
    }, ()).unwrap();

    let operator = Operator {
        tremolo: false,
        vibrato: false,
        sustaining: true,
        ksr: false,
        frequency_multiplier: 1,
        key_scale: 0,
        output_level_attenuation: 0,
        attack: 15,
        decay: 0,
        sustain: 0,
        release: 4,
        waveform: instrument::Waveform::Sine
    };
    let instrument = TwoOpInstrument {
        mode: instrument::TwoOpMode::Additive,
        feedback: 0,
        stereo_channels: instrument::StereoChannels::Both,
        operators: [operator.clone(), operator.clone()]
    };

    let instrument_ = instrument.clone();

    let output = VurpoSerialOPL3{
        port: serialport::new("/dev/ttyACM0", 115200).timeout(time::Duration::from_millis(10)).open().unwrap()
    };

    thread::spawn(move|| {
        let mut opl = OPL3::new(Box::new(output));
        let channelnum = opl.set_instruments(false, &[instrument.clone()], &[]).unwrap().1[0];
        println!("Channel {} used for our instrument", channelnum);
        loop {
            while let Ok(message) = rx.recv() {
                match message {
                    Message::MidiMessage(message) => {
                        if message[0] == 0x80 || (message[0] == 0x90 && message[1] == last_key && message[2] == 0) {
                            opl.note_off(channelnum);
                        } else if message[0] == 0x90 && message[2] != 0 {
                            last_key = message[1];
                            if let Some((block, f_number)) = key_number_to_f_number(message[1]) {
                                opl.note_off(channelnum);
                                opl.set_channel_note(channelnum, true, block, f_number);
                            }
                        }
                    },
                    Message::InstrumentMessage(instrument) => {
                        opl.set_twoop_instrument(channelnum, &instrument);
                    }
                }
            }
        }
    });
    
    let app = gui::OplApp{
        message_tx: tx,
        instrument: instrument_
    };
    let native_options = eframe::NativeOptions::default();
    eframe::run_native(Box::new(app), native_options);

}
