
#[derive(Debug, Clone, Copy)]
pub enum Waveform {
    Sine = 0,
    HalfSine = 1,
    AbsoluteSine = 2,
    PulseSine = 3,
    EvenSine = 4,
    EvenAbsoluteSine = 5,
    Square = 6,
    DerivedSquare = 7
}

#[derive(Clone, Debug)]
pub struct Operator {
    pub tremolo: bool,
    pub vibrato: bool,
    pub sustaining: bool,
    pub ksr: bool,
    pub frequency_multiplier: u8,       // 4 bits

    pub key_scale: u8,                  // 2 bits
    pub output_level_attenuation: u8,   // 6 bits

    pub attack: u8,     // 4 bits
    pub decay: u8,      // 4 bits

    pub sustain: u8,    // 4 bits
    pub release: u8,    // 4 bits

    pub waveform: Waveform
}

#[derive(Clone, Copy, Debug)]
pub enum StereoChannels {
    None = 0,
    L = 1,
    R = 2,
    Both = 3
}

#[derive(Clone, Copy, Debug)]
pub enum TwoOpMode {    // 2-operator modes:
    Fm = 0,             // Op * Op
    Additive = 1        // Op + Op
}

#[derive(Clone, Debug)]
pub struct TwoOpInstrument {
    pub mode: TwoOpMode,
    pub operators: [Operator; 2],
    pub stereo_channels: StereoChannels,
    pub feedback: u8 // 3 bits
}

#[derive(Clone, Copy, Debug)]
pub enum FourOpMode {   // 4-operator modes:
    FmFmFm,             // Op * Op * Op * Op
    AdditiveFmFm,       // Op + (Op * Op * Op)
    FmAdditiveFm,       // (Op * Op) + (Op * Op)
    AdditiveFmAdditive  // Op + (Op * Op) + Op
}

#[derive(Clone, Debug)]
pub struct FourOpInstrument {
    pub mode: FourOpMode,
    pub operators: [Operator; 4],
    pub stereo_channels: StereoChannels,
    pub feedback: u8 // 3 bits
}

#[derive(Clone, Debug)]
pub struct PercussionInstrument {}