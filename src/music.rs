// F-numbers for C, C#, D, Eb, E, F, F#, G, Ab, A, Bb, B
pub const F_NUMBERS_SCALE: [u16; 12] = [342, 363, 385, 408, 432, 458, 485, 514, 544, 577, 611, 647];

// 440 Hz -> F-number for A, block number 4!